package com.fincity.car_search.controllers;

import com.fincity.car_search.TransferObject;
import com.fincity.car_search.domain.entity.Car;
import com.fincity.car_search.domain.repository.CarRepository;
import com.fincity.car_search.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/cars")
public class CarController {
    @Autowired
    CarService carService;

    @PostMapping(path = "/")
    public TransferObject<Car> addCar(HttpServletRequest request, @RequestBody Car input) {
        return new TransferObject<>(carService.addCar(input));
    }

    @PutMapping(path = "/{id}")
    public TransferObject<Car> updateCar(@RequestBody Car input, @PathVariable("id") Long id,HttpServletRequest request) {
        return  new TransferObject<>(carService.updateCar(input, id)).add(new Link(request.getRequestURI()));
    }
    @GetMapping(path = "/{id}")
    public TransferObject<Car> getCarById(@PathVariable("id") Long id,HttpServletRequest request) {
        return  new TransferObject<>(carService.findById(id)).add(new Link(request.getRequestURI()));
    }

    @DeleteMapping(path = "/{id}")
    public TransferObject<Boolean> deleteCar(@PathVariable("id") Long id) {
        return  new TransferObject<>(carService.deleteCar(id));
    }

    @GetMapping(path = "/")
    public TransferObject<List<Car>> searchCar(@RequestParam(required = false, name = "name") String name,
                               @RequestParam(required = false, name = "model") String model,
                               @RequestParam(required = false, name = "manufacture") String manufacture, @RequestParam(required = false, name = "year") Integer year,
                               @RequestParam(required = false, name = "color") String color,
                               @RequestParam(required = false, name = "isAndOnColor") boolean isAnd,HttpServletRequest request) {


        return new TransferObject<>(carService.searchCar(name,model,manufacture,year,color,isAnd)).add(new Link(request.getRequestURI()));

    }
}
