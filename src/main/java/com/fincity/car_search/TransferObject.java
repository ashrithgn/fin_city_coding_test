package com.fincity.car_search;




import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class TransferObject<T>  extends RepresentationModel<TransferObject<T>> {
   public T data;

    public TransferObject( T data) {
        this.data = data;
    }

}
