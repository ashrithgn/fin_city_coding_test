package com.fincity.car_search.service;

import com.fincity.car_search.domain.entity.Car;
import com.fincity.car_search.domain.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {
    @Autowired
    private CarRepository carRepository;

    public Car addCar(Car input) {
        return carRepository.addCar(input);
    }

    public Car findById(Long id) {
        return carRepository.findById(id);
    }

    public Car updateCar(Car input, Long id) {
        return carRepository.updateCar(input, id);
    }

    public boolean deleteCar(Long id) {
        return carRepository.deleteCar(id);
    }

    public List<Car> searchCar(String name, String model, String manufacture, Integer year, String color, boolean isAnd) {
        List<Car> cars = carRepository.getAllCars();

        List<Car> filteredCars = new ArrayList<>(cars);

        if (name != null) {
            filteredCars = filteredCars.parallelStream().filter(car -> car.getName().equals(name)).collect(Collectors.toList());
        }

        if (model != null) {
            filteredCars = filteredCars.parallelStream().filter(car -> car.getModel().equals(model)).collect(Collectors.toList());
        }

        if (manufacture != null) {
            filteredCars = filteredCars.parallelStream().filter(car -> car.getManufactureName().equals(manufacture)).collect(Collectors.toList());
        }

        if (year != null) {
            filteredCars = filteredCars.parallelStream().filter(car -> car.getYear().equals(year)).collect(Collectors.toList());
        }

        if(color != null) {
            if(isAnd) {
                filteredCars = filteredCars.parallelStream().filter(car -> car.getColor().equals(color)).collect(Collectors.toList());
            } else {
                filteredCars.addAll(cars.parallelStream().filter(car -> car.getColor().equals(color)).collect(Collectors.toList()));
                filteredCars = filteredCars.stream().collect(Collectors.toSet()).stream().collect(Collectors.toList());
            }
        }

        return filteredCars;

    }
}
