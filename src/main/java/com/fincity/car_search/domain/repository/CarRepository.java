package com.fincity.car_search.domain.repository;

import com.fincity.car_search.domain.data_store.CarStore;
import com.fincity.car_search.domain.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CarRepository {
    @Autowired
    private CarStore store;

    public Car findById(Long id) {
        if (store.getStoreObject().containsKey(id)) {
            return store.getStoreObject().get(id);
        }
        return null;
    }

    public List<Car> getAllCars() {
        return store.getStoreObject()
                .values()
                .stream()
                .collect(Collectors.toList());
    }

    public Car addCar(Car input) {
        input.setId(store.getPrimaryKey());
        store.getStoreObject().put(input.getId(), input);
        return input;
    }

    public Car updateCar(Car input, Long id) {
        if (store.getStoreObject().containsKey(id)) {
            input.setId(id);
            store.getStoreObject().put(id, input);
            return input;
        }
        throw new RuntimeException("invalid record");
    }


    public boolean deleteCar(Long id) {
        if (store.getStoreObject().containsKey(id)) {
            store.getStoreObject().remove(id);
            return true;
        }
        throw new RuntimeException("invalid record");
    }
}
