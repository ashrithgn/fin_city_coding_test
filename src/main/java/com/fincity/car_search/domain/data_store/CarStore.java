package com.fincity.car_search.domain.data_store;

import com.fincity.car_search.domain.entity.Car;
import lombok.Synchronized;
import org.springframework.stereotype.Component;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class CarStore {
    private Map<Long, Car> store =  new ConcurrentHashMap<>();
    private Long nextVal = 0L;

    public Map<Long,Car> getStoreObject() {
       return store;
    }

    @Synchronized
    public Long getPrimaryKey() {
        nextVal = nextVal + 1;
        return nextVal;
    }
}
