## Requirement
Use spring security , session management , ability to login (Email ID and password)
* A Car controller class , which defines all the HTTP methods and perform CRUD
operations
* A car model , which has below characteristics
* Id
* Name
* Manufacture Name
* Model
* Manufacturing year
* Color
* Put dependencies in gradle file
* Create MySQL tables using .sql file , which must be part of the project
* Ability to search car/cars by name , manufacture name , model , manufacturing year
and/or color.

### Run Application
* ./gradlew build
* ./gradlew bootWar

* open browser and naviagte to http://localhost:8080/swagger-ui.html

* and login will be prompted login with user as user name and auto generated password provide in console

* use swagger docs to access the api


#### note i have not implemented spring security properly as i have never used spring security.
to Authorize rest api i have used custom implementation.
links for smple implementation
https://blogs.ashrithgn.com/implementing-jwt-token-to-authorise-rest-api-in-spring-boot/

### have provided swageer docs instead of postman collection